﻿using Microsoft.EntityFrameworkCore;

namespace WebTest2.Models.Entity
{
    public class WebDBContext : DbContext
    {
        public WebDBContext(DbContextOptions<WebDBContext> options) : base(options)
        {
        }

        public DbSet<Accounts> Accounts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // 設定SQLite連接字串
            //optionsBuilder.UseSqlite("Data Source=Models/WebDb.db");
            SQLitePCL.Batteries.Init();
        }
    }
}
