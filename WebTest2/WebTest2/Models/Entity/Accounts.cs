﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebTest2.Models.Entity
{
    /// <summary>
    /// dotnet ef migrations add InitialCreate_20231205-2 --project WebTest2 -o Models/Migrations
    /// dotnet ef database update --project WebTest2
    /// </summary>
    public class Accounts
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(10)]
        [Display(Name = "姓名")]
        public string Name { get; set; } = string.Empty;

        [Required]
        [MaxLength(20)]
        [Display(Name = "帳號")]
        public string Account { get; set; } = string.Empty;

        [Required]
        [MaxLength(256)]
        [Display(Name = "密碼")]
        public string Password { get; set; } = string.Empty;

        [MaxLength(100)]
        [Display(Name = "電子郵件")]
        public string Email { get; set; } = string.Empty;

        [MaxLength(15)]
        [Display(Name = "電話號碼")]
        public string PhoneNumber { get; set; } = string.Empty;

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateTime { get; set; }

        [Display(Name = "更新時間")]
        public DateTime? UpdateTime { get; set; }
    }
}
