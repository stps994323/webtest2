﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebTest2.Models.Migrations
{
    public partial class InitialCreate_202312053 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Account",
                table: "Accounts",
                type: "TEXT",
                maxLength: 20,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Account",
                table: "Accounts");
        }
    }
}
