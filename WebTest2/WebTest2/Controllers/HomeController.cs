﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebTest2.Models;
using WebTest2.Models.Entity;

namespace WebTest2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly WebDBContext _webDBContext;

        public HomeController(ILogger<HomeController> logger, WebDBContext webDBContext)
        {
            _logger = logger;
            _webDBContext = webDBContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}