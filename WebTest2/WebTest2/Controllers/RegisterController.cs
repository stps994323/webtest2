﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebTest2.Models.Entity;
using System.Threading.Tasks;

namespace WebTest2.Controllers
{
    public class RegisterController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly WebDBContext _webDBContext;

        public RegisterController(UserManager<IdentityUser> userManager, WebDBContext webDBContext)
        {
            _userManager = userManager;
            _webDBContext = webDBContext;


        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(Accounts model)
        {
            if (ModelState.IsValid)
            {
                // 撈取資料比對是否存在                
                var list = _webDBContext.Accounts.Where(w => w.Account == model.Account ).ToList();
                
                if (list.Count > 0) 
                {
                    return Content("帳號已重複");
                }

                // 驗證密碼長度、Email正確性 (驗證長度請寫成共用功能使用)

                // 密碼要加湊為sha256格式 (加密方式如上, 寫成共用) 

                // 新增
                model.CreateTime = DateTime.Now;
                _webDBContext.Accounts.Add(model);

                _webDBContext.SaveChanges();
                
                return RedirectToAction("Index", "Home");

                //var user = new IdentityUser { UserName = model.Account, Email = model.Email };
                //var result = await _userManager.CreateAsync(user, model.Password);

                //    if (result.Succeeded)
                //    {
                //        // 自動登入用戶，或者你可以將用戶導向登入頁面
                //        //await _signInManager.SignInAsync(user, isPersistent: false);
                //        return RedirectToAction("Index", "Home");
                //    }
                //    else
                //    {
                //        foreach (var error in result.Errors)
                //        {
                //            ModelState.AddModelError(string.Empty, error.Description);
                //        }
                //    }
            }

            return View(model);
        }
    }
}
